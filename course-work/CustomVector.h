#ifndef CUSTOM_VECTOR
#define CUSTOM_VECTOR


template<typename T>
class CustomVector {
private:
	T* data = nullptr;
	size_t size = 0;
	size_t capacity = 0;

	void realloc(size_t newcapacity);
public:
	class Iterator;

	CustomVector();
	CustomVector(const CustomVector<T>& other);

	void PushBack(const T& element);
	void PopBack();
	void Remove(int index);
	void Clear();
	T& At(int index);

	size_t GetSize();
	size_t GetCapacity();

	Iterator begin();
	Iterator end();

	CustomVector<T>& operator = (const CustomVector<T>&other);

	~CustomVector();
};
template<typename T>
class CustomVector<T>::Iterator {
private:
	T* ptr;
public:
	Iterator(T* p) : ptr(p) {}

	bool operator==(const Iterator& it) {
		return ptr == it.ptr;
	}
	bool operator!=(const Iterator& it) {
		return !(ptr == it.ptr);
	}
	T& operator*() {
		return *ptr;
	}
	T* operator->() {
		return ptr;
	}
	T& operator+(int n) {
		return *(ptr + n);
	}
	T& operator-(int n) {
		return*(ptr - n);
	}
	T& operator++() {
		ptr++;
		return *ptr;
	}
	T& operator--() {
		ptr--;
		return +ptr;
	}
	T& operator++(int) {
		Iterator it = *this;
		++(*this);
		return it;
	}
	T& operator--(int) {
		Iterator it = *this;
		--(*this);
		return it;
	};
};

//
// implementations
//

template<typename T>
void CustomVector<T>::realloc(size_t newcapacity) {
	if (newcapacity < size)
		newcapacity = size;

	T* newData = new T[newcapacity];

	for (int i = 0; i < size; i++)
		newData[i] = data[i];

	delete[] data;

	data = newData;
	capacity = newcapacity;
}
template<typename T>
inline CustomVector<T>::CustomVector() {}
template<typename T>
inline CustomVector<T>::CustomVector(const CustomVector<T>& other) {
	size = other.size;
	capacity = other.capacity;

	data = new T[size];

	for (int i = 0; i < size; i++) {
		data[i] = other.data[i];
	}
}
template<typename T>
inline void CustomVector<T>::PushBack(const T& element) {
	if (size >= capacity)
		realloc(capacity + 10);

	data[size] = element;
	size++;
}
template<typename T>
inline void CustomVector<T>::PopBack() {
	if (size == 0)
		throw std::exception("Tried to pop back an empty vector");
	size--;
}
template<typename T>
inline void CustomVector<T>::Remove(int index) {
	if (index < 0 || index > size)
		throw std::exception("index out of range when removing");
	for (int i = index; i < size - 1; i++) {
		data[i] = data[i + 1];
	}
	
	--size;
}
template<typename T>
inline void CustomVector<T>::Clear() {
	size = 0;
	capacity = 0;
	delete[]data;
	data = nullptr;
}
template<typename T>
inline T& CustomVector<T>::At(int index)
{
	return data[index];
}
template<typename T>
inline size_t CustomVector<T>::GetSize()
{
	return size;
}
template<typename T>
inline size_t CustomVector<T>::GetCapacity()
{
	return capacity;
}
template<class T>
inline typename CustomVector<T>::Iterator CustomVector<T>::begin()
{
	return Iterator(data);
}
template<class T>
inline typename CustomVector<T>::Iterator CustomVector<T>::end() {
	return Iterator(data + size);
}
template<typename T>
inline CustomVector<T>& CustomVector<T>::operator=(const CustomVector<T>& other) {
	if (this != other) {
		delete[]data;
		size = other.size;
		capacity = other.capacity;
		data = new T[size];

		for (int i = 0; i < size; i++)
			data[i] = other.data[i];
	}
	return *this;
}
template<class T>
inline CustomVector<T>::~CustomVector() {
	delete[]data;
}
#endif