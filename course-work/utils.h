#ifndef UTILS
#define UTILS

#include <ctime>
#include <string>


// converts std::string to tm*. Format: dd.mm.yyyy hh:mm
void strTotm(std::string& str, tm* t);
// converts tm* to std::string. Format: dd.mm.yyyy hh:mm. If time is nullptr, returns empty string
std::string tmTostr(tm* time);
// assigns current time to tm instance
void setTimeCur(tm& time);
bool IsNum(std::string);
int getInt();
#endif 
