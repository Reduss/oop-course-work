#ifndef CONSOLE_UI
#define CONSOLE_UI

#include "Shop.h"
#include <string>

class ConsoleUI {
private:
	Shop& shop;
public:
	ConsoleUI(Shop& sh);
	void MainMenu();
	void OutputInputDelimiter();

	void DisplayShop();
	void DisplayToSellList();
	void DisplayOrderList();


	void DisplayWarehouseList();
	void DisplayWarehouse();

	void DisplayInvoice(Invoice& inv);
	void DisplayInvoiceMenu();

	void DisplayBudgetInfo();

	void ChoosePrompt();
	void ConsoleClear();

	void DisplayProduct(Product& p, int index);
	void DisplayShopProduct(ShopProduct& p, int index);
};

#endif 
