#define _CRT_SECURE_NO_WARNINGS

#include "FileLogger.h"
#include "ShopProduct.h"
#include "utils.h"
#include "Shop.h"

FileLogger::FileLogger(std::string prodPath, std::string invPath, std::string prodInvPath,
	std::string _shopPath, std::string _warehouses) :
	products(prodPath), invoices(invPath), invoiceProducts(prodInvPath), shopPath(_shopPath), warehouses(_warehouses) { }
FileLogger::FileLogger(std::string prodpath, std::string _warehousePath)
	: products(prodpath), warehousePath(_warehousePath) { }
void FileLogger::LoadShopProducts(CustomVector<ShopProduct*>& vec) {

	char str[256];
	std::fstream f;
	char* token;

	std::string title, deliveryDate, expiryDate, unit;
	bool quality;
	int price, amount, expire, markup;

	std::string path = this->shopPath + this->products;
	f.open(path, std::ios::in);
	if (!f.is_open())
		throw std::exception("Couldn't open the shop products folder to load");
	f.getline(str, 256); // skip the first line with the name of the shop
	while (f.getline(str, 256)) {
		token = strtok(str, ",");
		title = token;
		token = strtok(NULL, ",");
		deliveryDate = token;
		token = strtok(NULL, ",");
		expiryDate = token;
		token = strtok(NULL, ",");
		quality = atoi(token);
		token = strtok(NULL, ",");
		price = atoi(token);
		token = strtok(NULL, ",");
		// original price
		token = strtok(NULL, ",");
		unit = token;
		token = strtok(NULL, ",");
		amount = atoi(token);
		token = strtok(NULL, ",");
		expire = atoi(token);
		token = strtok(NULL, ",");
		markup = atoi(token);

		vec.PushBack(new ShopProduct(title, deliveryDate, expiryDate,
			quality, price, unit, amount, expire, markup));
	}
	f.close();
}
void FileLogger::LoadWarehouseProducts(CustomVector<Product*>& vec) {

	char str[256];
	std::fstream f;
	char* token;

	std::string title, deliveryDate, expiryDate, unit;
	bool quality;
	int price, amount, expire;
	std::string path = this->warehousePath + this->products;

	f.open(path, std::ios::in);
	if (!f.is_open())
		throw std::exception("Couldn't open the warehosue products folder to load");
	while (f.getline(str, 256)) {
		token = strtok(str, ",");
		title = token;
		token = strtok(NULL, ",");
		deliveryDate = token;
		token = strtok(NULL, ",");
		quality = atoi(token);
		token = strtok(NULL, ",");
		price = atoi(token);
		token = strtok(NULL, ",");
		unit = token;
		token = strtok(NULL, ",");
		amount = atoi(token);
		token = strtok(NULL, ",");
		expire = atoi(token);

		vec.PushBack(new Product(title, deliveryDate,
			quality, price, unit, amount, expire));
	}
	f.close();
}
void FileLogger::SaveShopProducts(CustomVector<ShopProduct*>& prods, std::string shopName) {
	std::ofstream f;

	std::string path = this->shopPath + this->products;

	f.open(path, std::ios::out);

	if (!f.is_open())
		throw std::exception("couldn't open the products folder to save");
	f << shopName << std::endl;
	for (auto i : prods) {
		f << i->GetTitle() << "," << i->GetDeliveryDate() << "," << i->GetExpiryDate()
			<< "," << i->GetQuality() << "," << i->GetOriginalPrice() << "," << i->GetPrice() << ","
			<< i->GetUnits() << "," << i->GetAmount() << "," << i->GetExpiry() << "," << i->GetMarkup() << std::endl;
	}
	f.close();
}
void FileLogger::SaveWarehouseProducts(CustomVector<Product*>& prods) {
	std::ofstream f;

	std::string path = this->warehousePath + this->products;
	f.open(path);

	if (!f.is_open())
		throw std::exception("couldn't open the warehouse folder to save");
	
	for (auto i : prods) {
		f << i->GetTitle() << "," 
			<< i->GetDeliveryDate() << "," 
			<< i->GetQuality() << "," 
			<< i->GetPrice() << "," 
			<< i->GetUnits() << "," 
			<< i->GetAmount() << ","
			<< i->GetExpiry() << ","
			<< std::endl;
	}
	f.close();
}
void FileLogger::LoadWarehouseList(CustomVector<std::string>& warehouses) {
	std::ifstream f;

	std::string wareName;
	std::string path = this->warehouses;
	f.open(path);
	if (!f.is_open())
		throw std::exception("couldn't open the warehouse list");
	while (std::getline(f, wareName)) {
		warehouses.PushBack(wareName);
	}
	f.close();
}
void FileLogger::LoadInvoices(CustomVector<Invoice*>& vec) {

	std::ifstream f;
	char str[256];
	char* token;

	std::string title, sellerName, buyerName;
	int _id;
	double price = 0;
	int amount = 0;
	double sum = 0;

	std::string path = this->shopPath + this->invoices;
	if (FileLogger::IsEmpty(path))
		return;
	f.open(path);
	if (!f.is_open())
		throw std::exception("couldn't open the invoice folder to load");

	while (f.getline(str, 256)) {

		tm* _date = new tm();

		token = strtok(str, ",");
		_id = atoi(token);
		token = strtok(NULL, ",");
		sellerName = token;
		token = strtok(NULL, ",");
		buyerName = token;
		token = strtok(NULL, ",");
		strTotm(std::string(token), _date);
		token = strtok(NULL, ",");

		vec.PushBack(new Invoice(sellerName, buyerName, _date));
		delete _date;
		_date = nullptr;
	}
	f.close();


	path = this->shopPath + this->invoiceProducts;

	f.open(path);
	if (!f.is_open())
		throw std::exception("couldn't open the invoice folder to load products");
	while (f.getline(str, 256)) {
		token = strtok(str, ",");
		_id = atoi(token);
		token = strtok(NULL, ",");
		title = token;
		token = strtok(NULL, ",");
		price = atoi(token);
		token = strtok(NULL, ",");
		amount = atoi(token);

		for (int i = 0; i < vec.GetSize(); i++)
			if (vec.At(i)->GetId() == _id) {
				vec.At(i)->AddProduct(new InvoiceProduct(title, amount, price));
			}
	}
	f.close();
}
void FileLogger::SaveInvoices(CustomVector<Invoice*>& vec) {
	std::ofstream f;

	std::string path = this->shopPath + this->invoices;

	f.open(path, std::ios::out);


	// save invoices
	if (!f.is_open())
		throw std::exception("couldn't open the invoice folder to save invoices");
	for (auto i : vec) {
		f << i->GetId() << "," << i->GetSeller() << "," << i->GetBuyer() << "," << i->GetDate() << std::endl;
	}
	f.close();

	// save invoices
	path = this->shopPath + this->invoiceProducts;

	f.open(path);
	if (!f.is_open())
		throw std::exception("couldn't open the invoice folder to save products");
	for (auto i : vec) {
		for (int j = 0; j < i->Size(); j++) {
			f << i->GetId() << "," << i->GetProduct(j)->GetTitle() << "," << i->GetProduct(j)->GetPrice()
				<< "," << i->GetProduct(j)->GetAmount() << std::endl;
		}
	}
	f.close();
}
bool FileLogger::IsEmpty(std::string& path) {
	std::ifstream f(path);
	if (f.peek() == EOF)
		return true;
	return false;

}
void FileLogger::SaveShopName(Shop& s) {
	std::ofstream f;
	std::string path = this->shopPath + this->products;

	f.open(path, std::ios::out);
	// save invoices
	if (!f.is_open())
		throw std::exception("couldn't open the invoice folder to save invoices");
	f << s.GetName();
	f.close();
}
std::string& FileLogger::GetName()
{
	std::ifstream f;
	f.open("course-work/Data/Shop/products.txt");
	std::string* str = new std::string;
	if (!f.is_open())
		throw std::exception("Couldn't open the file to read the name");
	std::getline(f, *str);
	return *str;
}
