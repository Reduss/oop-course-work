#include "ConsoleUI.h"

#include <iostream>
#include <iomanip>

ConsoleUI::ConsoleUI(Shop& sh) : shop(sh) {}

void ConsoleUI::MainMenu() {
	std::cout << "+==========|" << shop.GetName() << "|===========+\n";
	std::cout << "1 - Manage shop\n2 - View Warehouse\n3 - Invoices\n4 - Budget\n";
	std::cout << "+" << std::setfill('=') << std::setw(23 + shop.GetName().length()) << "=" << std::setfill(' ')
		<< "+" << std::setw(1) <<  std::endl;
}
void ConsoleUI::OutputInputDelimiter() {
	std::cout << "\n------------------------------\n";
}
void ConsoleUI::DisplayShop() {
	if (shop.GetProducts().GetSize() == 0) {
		std::cout << "The shop is empty.\n";
		return;
	}
	std::cout << " |" << std::setfill(' ')
		<< std::setw(15) <<std::left << "Product" << "|"
		<< std::setw(10) << std::left << "Warehouse price" << "|"
		<< std::setw(7) << std::left << "Price" << "|"
		<< std::setw(9) << std::left << "Markup(%)" << "|"
		<< std::setw(6) << std::left << "Amount" << "|"
		<< std::setw(5) << std::left << "Units" << "|"
		<< std::setw(7) << std::left << "Quality" << "|"
		<< std::setw(16) << std::left << "Delivery date" << "|"
		<< std::setw(16) << std::left << "Expiry Date" << "|"
		<< std::endl;
	std::cout << " +" << std::setfill('-') << std::setw(104) << "-" << std::setfill(' ') << "+" << std::endl;
	for (int i = 0; i < shop.GetProducts().GetSize(); i++)
		DisplayShopProduct(*shop.GetProducts().At(i), i + 1);
}
void ConsoleUI::DisplayToSellList() {
	std::cout << std::setfill('=') << std::setw(30) << "=" << std::setfill(' ') << std::endl;
	std::cout << "To Sell(" << shop.GetToSellList().GetSize() << "): \n";
	if (shop.GetToSellList().GetSize() != 0) {
		for (int i = 0; i < shop.GetToSellList().GetSize(); i++) {
			std::cout << " +" << std::setfill('-') << std::setw(104) << "-" << std::setfill(' ') << "+" << std::endl;
			DisplayShopProduct(*shop.GetToSellList().At(i), i + 1);
		}
	}
	std::cout << std::setfill('=') << std::setw(30) << "=" << std::setfill(' ') << std::endl;
}
void ConsoleUI::DisplayOrderList() {
	std::cout << std::setfill('=') << std::setw(30) << "=" << std::setfill(' ') << std::endl;
	std::cout << "Order(" << shop.GetOrderList().GetSize() << "): \n";
	if (shop.GetOrderList().GetSize() != 0) {
		std::cout << " +" << std::setfill('-') << std::setw(61) << "-" << std::setfill(' ') << "+" << std::endl;
		for (int i = 0; i < shop.GetOrderList().GetSize(); i++)
			DisplayProduct(*shop.GetOrderList().At(i), i + 1);
	}
	std::cout << std::setfill('=') << std::setw(30) << "=" << std::setfill(' ') << std::endl;
}
void ConsoleUI::DisplayWarehouseList() {
	for (int i = 0; i < shop.GetWarehouses().GetSize(); i++) {
		std::cout << i + 1 << " - " << shop.GetWarehouses().At(i) << std::endl;
	}
}
void ConsoleUI::DisplayWarehouse() {
	if (shop.GetWarehouse().GetSize() == 0) {
		std::cout << "The warehouse is empty.\n";
		return;
	}
	std::cout << " |"
		<< std::setw(15) << std::left << "Product" << "|"
		<< std::setw(7) << std::left << "Price" << "|"
		<< std::setw(6) << std::left << "Amount" << "|"
		<< std::setw(5) << std::left << "Units" << "|"
		<< std::setw(7) << std::left << "Quality" << "|"
		<< std::setw(16) << std::left << "Delivery date" << "|"
		<< std::endl;
	std::cout << " +" << std::setfill('-') << std::setw(61) << "-" << std::setfill(' ') << "+" << std::endl;
	for (int i = 0; i < shop.GetWarehouse().GetCollection().GetSize(); i++)
		DisplayProduct(*(shop.GetWarehouse().GetCollection().At(i)), i + 1);
}
void ConsoleUI::DisplayInvoice(Invoice& inv) {
	// +-----------------------------------------------+
	//  id:
	//  date: 
	//  Seller:                                       
	//  Buyer:                                        
	//  Products:
	// 		prodinfo
	//		prodinfo
	//		prodinfo
	//  Products total:
	//  Total sum:
	// +-----------------------------------------------+
	std::cout << "+===============================+\n";
	std::cout << "Id: " << inv.GetId() << std::endl
		<< "Date: " << inv.GetDate() << std::endl
		<< "Seller: " << inv.GetSeller() << std::endl
		<< "Buyer: " << inv.GetBuyer() << std::endl
		<< "Products:" << std::endl;
	std::cout << " |"
		<< std::setw(15) <<std::left << "Product" << "|" 
		<< std::setw(7) << std::left << "Price" << "|"
		<< std::setw(6) << std::left << "Amount" << "|"
		<< std::endl;
	std::cout << "+------------------------------+\n";
	for (int i = 0; i < inv.GetCollection().GetSize(); i++) {
		std::cout << i + 1 << "|" 
			<< std::setw(15) << std::left << inv.GetProduct(i)->GetTitle() << "|"
			<< std::setw(7) << std::left << inv.GetProduct(i)->GetPrice() << "|"
			<< std::setw(6) << std::left << inv.GetProduct(i)->GetAmount() << "|"
			<< std::endl;
	}
	std::cout << "+------------------------------+\n";
	std::cout << "Amount of items: " << inv.GetTotalAmount() << std::endl;
	std::cout << "Total price: " << inv.GetTotalPrice() << std::endl;
	std::cout << "+===============================+\n";

}
void ConsoleUI::DisplayInvoiceMenu() {
	for (auto i : shop.GetInvoices())
		DisplayInvoice(*i);
}
void ConsoleUI::DisplayBudgetInfo() {
	ConsoleClear();
	std::cout << "+==========|" << shop.GetName() << "|===========+\n";
	std::cout << "This month's budget info:\n";
	std::cout << "-Expences: " << shop.GetExpences() << std::endl
		<< "-Earnings: " << shop.GetEarnings() << std::endl
		<< "-Profit: " << shop.GetProfit() << std::endl;
	std::cout << "+" << std::setfill('=') << std::setw(23 + shop.GetName().length()) << "=" << std::setfill(' ')
		<< "+" << std::setw(1) << std::endl;
}
void ConsoleUI::ChoosePrompt() {
	OutputInputDelimiter();
	std::cout << "Enter the options' number to choose it. 0 - back\n";
}
void ConsoleUI::ConsoleClear() {
	std::system("cls");
}
void ConsoleUI::DisplayProduct(Product& p, int index) {
	std::cout << index << "|"
		<< std::setw(15) << std::left << p.GetTitle() << "|"
		<< std::setw(7) << std::left << p.GetPrice() << "|"
		<< std::setw(6) << std::left << p.GetAmount() << "|"
		<< std::setw(5) << std::left << p.GetUnits() << "|"
		<< std::setw(7) << std::left << p.GetQualityStr() << "|"
		<< std::setw(16) << std::left << p.GetDeliveryDate() << "|\n";
	std::cout << " +" << std::setfill('-') << std::setw(61) << "-" << std::setfill(' ') << "+" << std::endl;
}
void ConsoleUI::DisplayShopProduct(ShopProduct& p, int index) {
	// title - orig price - price - amount - units - quality - dateofdelivery - date of expiry
	std::cout << index << "|" 
		<< std::setw(15) << std::left << p.GetTitle() << "|"
		<< std::setw(15) << std::left << p.GetOriginalPrice() << "|"
		<< std::setw(7) << std::left << p.GetPrice() << "|"
		<< std::setw(9) << std::left << p.GetMarkup() << "|"
		<< std::setw(6) << std::left << p.GetAmount() << "|"
		<< std::setw(5) << std::left << p.GetUnits() << "|"
		<< std::setw(7) << std::left << p.GetQualityStr() << "|"
		<< std::setw(16) << std::left << p.GetDeliveryDate() << "|"
		<< std::setw(16) << std::left << p.GetExpiryDate() << "|\n";
	std::cout << " +" << std::setfill('-') << std::setw(104) << "-" << std::setfill(' ') << "+" << std::endl;
}
