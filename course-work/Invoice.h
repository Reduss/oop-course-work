#ifndef INVOICE
#define INVOICE

#include "Product.h"
#include "InvoiceProduct.h"
#include "CustomVector.h"

#include <ctime>


class Invoice {
private:
	static int nextId;
	int id;									// id of the invoice
	CustomVector<InvoiceProduct*> products;	// products in the deal
	std::string sellerName;					// name of the seller
	std::string buyerName;					// name of the buyer
	tm* date;
	double totalSum;						// total amount of money spent on a deal
	int amountOfProducts;					// amount of products in the deal
		
	void SetAmountOfProducts();
	void setTotalSum();
public:
	Invoice(std::string seller, std::string buyer);
	Invoice(std::string seller, std::string buyer, tm* date);
	int GetId();
	std::string GetSeller();
	std::string GetBuyer();
	std::string GetDate();
	tm& GetTmDate();
	double GetTotalPrice();
	int GetTotalAmount();
	void AddProduct(InvoiceProduct* prod);
	CustomVector<InvoiceProduct*>& GetCollection();
	InvoiceProduct* GetProduct(int index);
	// returns the number of items in the collection
	int Size();
	~Invoice();
};
#endif 
