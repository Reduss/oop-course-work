#ifndef SHOP_PRODUCT
#define SHOP_PRODUCT

#include "Product.h"

// describes a product in a shop
class ShopProduct : public Product {
private:
	int markupPercentage;		// the amount added to price of a product in %
	tm* expiryDate;
	void setExpiryDate();
public:
	//default constuctor
	ShopProduct();
	// parameterized constructor, the delivery and expiry dates are set manualy
	// date format: dd.mm.yyyy hh:mm
	ShopProduct(std::string tit, std::string delivDate, 
		std::string expDate, bool quality, double price, std::string unit,
		int amount, int expire, int markup);
	// parameterized constructor, the delivery date is set to current, expiry date
	// is set automatically
	// date format: dd.mm.yyyy hh:mm
	ShopProduct(std::string tit, bool quality, double price, std::string unit, int amount, int expire);
	// Copy constructor
	ShopProduct(const ShopProduct& prod);
	virtual ~ShopProduct();

	// return the amount added to price of a product
	int GetMarkup();
	double GetPrice() override;
	// get the price from the warehouse
	double GetOriginalPrice();
	// returns a string representation of expiry date
	std::string GetExpiryDate();

	void SetMarkup(int val);


	ShopProduct& operator=(const ShopProduct& prod);
};
#endif 