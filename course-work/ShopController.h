#ifndef SHOP_CONTROLLER
#define SHOP_CONTROLLER

#include "Shop.h"
#include <string>
#include "ConsoleUI.h"

class ShopController {
private:
	Shop* shop = nullptr;
	ConsoleUI* ui = nullptr;

	void initialize();

public:
	void Start();
	void ShopMenu();
	void Sell();
	void MarkupMenu();

	void WarehouseListMenu();
	void WarehouseMenu();
	void MakeOrder();

	void InvoiceMenu();

	void BudgetInfo();


	bool OperationFinishPrompt();
};


#endif 
