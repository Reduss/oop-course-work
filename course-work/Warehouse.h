#ifndef WAREHOUSE
#define WAREHOUSE

#include "CustomVector.h"
#include "FileLogger.h"

#include <vector>

class Warehouse {
private:
	CustomVector<Product*> collection;
	FileLogger* logger;
	std::string name;
public:
	Warehouse(std::string name);
	Product* GetElement(int index);
	CustomVector<Product*>& GetCollection();
	void RemoveElement(int index);
	void AddElement(Product* prod);
	int GetSize();
	std::string GetName();

	void Load();
	void Save();


	~Warehouse();
};

#endif
