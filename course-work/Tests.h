#ifndef TESTS
#define TESTS

#include "Product.h"
#include "ShopProduct.h"
#include <exception>

void RunTests();


void TestProduct();
void TestShopProduct();
void TestPoly();
void TestVector();
void TestCustomVector();
void TestFileLogger();
void TestShop();
#endif 