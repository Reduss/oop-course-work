#include "ShopController.h"
#include "FileLogger.h"
#include "utils.h"

#include <fstream>



void ShopController::initialize() {
	std::string name(std::move(FileLogger::GetName()));
	if (name.empty()) {
		std::cout << "Enter the name of your shop:" << std::endl;
		std::getline(std::cin, name);
	}
	this->shop = new Shop(name);
	this->ui = new ConsoleUI(*shop);
}
void ShopController::Start() {
	initialize();
	while (1) {
		std::string str;
		ui->ConsoleClear();
		ui->MainMenu();
		
		int n = getInt();
		switch (n) {
		case 1:
			ShopMenu();
			break;
		case 2:
			WarehouseListMenu();
			break;
		case 3:
			InvoiceMenu();
			break;
		case 4:
			BudgetInfo();
			break;
		default:
			std::cout << "Incorrect input.\n";
			break;
		}
	}
}
void ShopController::ShopMenu() {
	ui->ConsoleClear();
	ui->DisplayShop();
	ui->OutputInputDelimiter();
	std::cout << "1 - choose products to sell. 2 - set markup. 0 - back.\n";
	int n;
	do {
		n = getInt();
		if (n == 1 && shop->GetProducts().GetSize() == 0) {
			std::cout << "No products to choose from\n";
			continue;
		}
		else if (n == 1 && shop->GetProducts().GetSize() != 0)
			Sell();
		else if (n == 2 && shop->GetProducts().GetSize() != 0)
			MarkupMenu();
		else if (n != 0) {
			std::cout << "Incorrect input\n";
			continue;
		}
		ui->ConsoleClear();
		ui->DisplayShop();
		ui->OutputInputDelimiter();
		std::cout << "1 - choose products to sell. 2 - set markup. 0 - back.\n";
	} while (n != 0);
}
void ShopController::Sell() {
	int n;
	bool flag = false;
	do {
		ui->ConsoleClear();
		ui->DisplayShop();
		ui->DisplayToSellList();
		ui->OutputInputDelimiter();
		std::cout << "Choose the product you want to sell. 0 to finish.\n";
		n = getInt();

		if (n < 0 || n > shop->GetProducts().GetSize()) {
			std::cout << "incorrect input.\n";
			continue;
		}
		else if (n != 0) {
			std::cout << "How much of " << shop->GetProducts().At(n - 1)->GetTitle() << " do you want to sell?";
			std::cout << " 0 to finish";
			ui->OutputInputDelimiter();
			while (1) {
				int amount = getInt();
				if (amount == 0)
					break;
				else if (shop->AddItemToSellList(n - 1, amount) == 1)
					break;
				else
					std::cout << "Invalid amount.\n";
			}
		}
		if (n == 0) {
			std::cout << "Are you finished?(1/0)\n";
			flag = OperationFinishPrompt();
		}
	} while (!flag);
	if (shop->GetToSellList().GetSize() == 0)
		return;
	std::cout << "Do you want to confirm the sale?(1/0)";
	ui->OutputInputDelimiter();
	if (OperationFinishPrompt()) {
		std::cout << "Enter the name of the buyer:" << std::endl;
		std::string name;
		std::cin.ignore();
		std::getline(std::cin, name);
		shop->Sell(name);
	}
	else 
		shop->CancelOrder();
}
void ShopController::MarkupMenu() {
	int n;
	do {
		ui->ConsoleClear();
		ui->DisplayShop();
		ui->OutputInputDelimiter();
		std::cout << "1 - choose products to set markup for.\n";
		n = getInt();
		if (n == 0)
			break;
		if (shop->GetProducts().GetSize() == 0) {
			std::cout << "No products to choose from\n";
			continue;
		}
		else if (n < 0 || n > shop->GetProducts().GetSize() + 1) {
			std::cout << "Incorrect input\n";
			continue;
		}
		else {
			std::cout << "Enter the markup for " << shop->GetProducts().At(n - 1)->GetTitle() << ":\n";
			int markup = getInt();
			shop->GetProducts().At(n - 1)->SetMarkup(markup);
			shop->SaveToFile();
		}
	} while (n != 0);
}
void ShopController::WarehouseListMenu() {
	ui->ConsoleClear();
	std::cout << "Choose the warehouse to work with:\n";
	ui->DisplayWarehouseList();
	ui->ChoosePrompt();
	int n;
	do {
		n = getInt();
		if (n < shop->GetWarehouses().GetSize() + 1 && n != 0) {
			shop->InitialyzeWarehouse(shop->GetWarehouses().At(n - 1));
			WarehouseMenu();
			ui->ConsoleClear();
			std::cout << "Choose the warehouse to work with:\n";
			ui->DisplayWarehouseList();
			ui->ChoosePrompt();
		}
		else if (n == 0)
			break;
		else
			std::cout << "Incorrect imput.\n";
	} while (n != 0);
}
void ShopController::WarehouseMenu() {
	ui->ConsoleClear();
	ui->DisplayWarehouse();
	ui->OutputInputDelimiter();
	std::cout << "1 - choose products to order. 0 - back.\n";
	int n;
	do {
		n = getInt();
		if (n == 1 && shop->GetWarehouse().GetSize() == 0) {
			std::cout << "No products to choose from\n";
			continue;
		}
		else if (n == 1 && shop->GetWarehouse().GetSize() != 0) {
			MakeOrder();
		}
			
		else if(n != 0) {
			std::cout << "Incorrect input\n";
			continue;
		}
		ui->ConsoleClear();
		ui->DisplayWarehouse();
		ui->OutputInputDelimiter();
		std::cout << "1 - choose products to order. 0 - back.\n";
	} while (n != 0);
}
void ShopController::MakeOrder() {
	
	int n;
	bool flag = false;
	do {
		ui->ConsoleClear();
		ui->DisplayWarehouse();
		ui->DisplayOrderList();
		ui->OutputInputDelimiter();
		std::cout << "Choose the product you want to order. 0 to finish.\n";
		n = getInt();

		if (n < 0 || n > shop->GetWarehouse().GetSize()) {
			std::cout << "incorrect input.\n";
			continue;
		}
		else if (n != 0) {
			ui->OutputInputDelimiter();
			std::cout << "How much of " << shop->GetWarehouse().GetElement(n - 1)->GetTitle() << " do you want to order?";
			std::cout << " 0 to finish\n";
			while (1) {
				int amount = getInt();
				if (amount == 0)
					break;
				else if (shop->AddItemToOrder(n - 1, amount) == 1)
					break;
				else 
					std::cout << "Invalid amount.\n";
			}
		}
		if (n == 0) {
			std::cout << "Are you finished?(1/0)\n";
			flag = OperationFinishPrompt();
		}
	} while (flag == false);
	if (shop->GetOrderList().GetSize() == 0)
		return;
	std::cout << "Do you want to confirm the order?(1/0)";
	ui->OutputInputDelimiter();
	if(OperationFinishPrompt())
		shop->OrderFromWarehouse();
	else {
		shop->CancelOrder();
	}
}
void ShopController::InvoiceMenu() {
	int n;
	do {
		ui->ConsoleClear();
		ui->DisplayInvoiceMenu();
		ui->OutputInputDelimiter();
		std::cout << "0 - back\n";
		n = getInt();
	} while (n != 0);

}
void ShopController::BudgetInfo() {
	shop->CalculateBudget(shop->GetCurentMonthInvoices());
	ui->DisplayBudgetInfo();
	ui->OutputInputDelimiter();
	std::cout << "0 - back\n";
	int n;
	do {
		n = getInt();
	} while (n != 0);
}
bool ShopController::OperationFinishPrompt() {
	int n = getInt();
	while (1) {
		if (n == 1)
			return true;
		if (n == 0)
			return false;
		std::cout << "Incorrect input.\n";
	}
}
