#include "Warehouse.h"

Warehouse::Warehouse(std::string name) : logger(nullptr), name(name) {
	
	
	std::string prodpath = this->name + "-products.txt";
	std::string warepath = "course-work/Data/Warehouse/";
	this->logger = new FileLogger(prodpath, warepath);
	this->logger->LoadWarehouseProducts(this->collection);
}

Product* Warehouse::GetElement(int index) {
	return this->collection.At(index);
}
CustomVector<Product*>& Warehouse::GetCollection() {
	return this->collection;
}
void Warehouse::RemoveElement(int index) {
	this->collection.Remove(index);
}

void Warehouse::AddElement(Product* prod) {
	this->collection.PushBack(prod);
}

int Warehouse::GetSize() {
	return this->collection.GetSize();
}

std::string Warehouse::GetName() {
	return this->name;
}

void Warehouse::Load() {
	this->logger->LoadWarehouseProducts(this->collection);
}

void Warehouse::Save() {
	this->logger->SaveWarehouseProducts(this->collection);
}

Warehouse::~Warehouse() {
	delete this->logger;
	for (int i = 0; i < collection.GetSize(); i++) {
		delete collection.At(i);
		collection.At(i) = nullptr;
	}
}
