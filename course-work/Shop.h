#ifndef SHOP
#define SHOP

#include "Product.h"
#include "ShopProduct.h"
#include "Warehouse.h"
#include "Invoice.h"
#include "FileLogger.h"
#include "CustomVector.h"

#include <string>
#include <vector>
#include <ctime>


class Shop {
private:
	std::string name;									// name of the shop
	CustomVector<ShopProduct*> collection;				// list of shop's products
	CustomVector<Invoice*> invoices;					// shop's orders and sales history
	Warehouse* warehouse;								// warehouses with which the shop can make deals
	FileLogger* logger;									// manages file i/o
	double moneySpent;									// money spent on purchasing from warehouse
	double moneyEarned;									// money earned by selling
	double profit;
	CustomVector<std::string> warehouses;				// names of availible warehouses
	CustomVector<Product*> currentOrder;				// products to order from a warehouse
	CustomVector<ShopProduct*> currentToSellList;		// products to sell to customers

public:
	Shop(std::string name);
	
	// load saved data from a file
	void LoadFromFile();
	// save the current state of the collection to a file
	void SaveToFile();

	ShopProduct& GetElement(int index);
	CustomVector<ShopProduct*>& GetProducts();
	CustomVector<ShopProduct*>& GetToSellList();
	CustomVector<Product*>& GetOrderList();
	Warehouse& GetWarehouse();
	CustomVector<Invoice*>& GetInvoices();
	CustomVector<std::string>& GetWarehouses();
	CustomVector<Invoice*>& GetCurentMonthInvoices();

	std::string& GetName();
	double GetExpences();
	double GetEarnings();
	double GetProfit();

	void CalculateBudget(CustomVector<Invoice*>& invs);
	// load chosen warehouse items from the file
	void InitialyzeWarehouse(std::string name);
	// adds an item to the order list from a warehouse. Returns 1 on success, 0 on failure
	int AddItemToOrder(int WarehouseItemIndex, int amount);
	// adds an item to the list of items to sale. Returns 1 on success, 0 on failure
	int AddItemToSellList(int ShopItemIndex, int amount);

	
	// fulfills the warehouse order
	int OrderFromWarehouse();
	// cancells the warehouse order
	void CancelOrder();
	// fulfills sale request
	void Sell(std::string buyerName);
	// cancells the sale request 
	void CancelSell();

	void SetName(std::string name);
	~Shop();
};


#endif 

