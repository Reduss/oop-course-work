#include "Invoice.h"
#include "utils.h"

int Invoice::nextId = 1;

void Invoice::SetAmountOfProducts() {
	this->amountOfProducts = 0;
	for (auto i : this->products)
		this->amountOfProducts += i->GetAmount();
}
void Invoice::setTotalSum() {
	double res = 0;
	for (int i = 0; i < this->products.GetSize(); i++) {
		res += this->products.At(i)->GetAmount() * this->products.At(i)->GetPrice(); 
	}
	this->totalSum = res;
}
Invoice::Invoice(std::string seller, std::string buyer, tm* date)
	:sellerName(seller), buyerName(buyer), date(nullptr), amountOfProducts(0),
	totalSum(0) {
	this->date = new tm();
	*(this->date) = *(date);
	this->id = nextId;
	nextId++;
	this->SetAmountOfProducts();
	this->setTotalSum();
}
Invoice::Invoice(std::string seller, std::string buyer) : sellerName(seller), buyerName(buyer), totalSum(0),
		amountOfProducts(0) {
	
	this->date = new tm;
	setTimeCur(*date);
	this->id = nextId;
	nextId++;
	this->SetAmountOfProducts();
	this->setTotalSum();
}
int Invoice::GetId() {
	return this->id;
}
std::string Invoice::GetSeller() {
	return this->sellerName;
}
std::string Invoice::GetBuyer() {
	return this->buyerName;
}
std::string Invoice::GetDate()
{
	return tmTostr(this->date);
}
tm& Invoice::GetTmDate() {
	return *this->date;
}
double Invoice::GetTotalPrice() {
	return this->totalSum;
}
void Invoice::AddProduct(InvoiceProduct* prod) {
	this->products.PushBack(prod);
	this->SetAmountOfProducts();
	this->setTotalSum();
}
int Invoice::GetTotalAmount()
{
	return this->amountOfProducts;
}
CustomVector<InvoiceProduct*>& Invoice::GetCollection() {
	return this->products;
}
InvoiceProduct* Invoice::GetProduct(int index)
{
	return this->products.At(index);
}
int Invoice::Size() {
	return this->products.GetSize();
}
Invoice::~Invoice() {
	std::cout << "\nInve dest called\n";
	for (auto i : products) {
		delete i;
	}
	products.Clear();
}
