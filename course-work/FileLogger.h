#ifndef FILE_LOGGER
#define FILE_LOGGER

#include <string>
#include <fstream>

#include "ShopProduct.h"
#include "CustomVector.h"
#include "Invoice.h"

class Shop;

class FileLogger {
private:
	std::string products;					// name of the file with products
	std::string invoices;					// name of the file with invoices
	std::string invoiceProducts;			// name of the file with invoice id and products
	std::string warehouses;					// names of warehouses
	std::string shopPath;					// path to the shop files
	std::string warehousePath;				// path to the warehouse files
	
public:
	// constructor for a shop
	FileLogger(std::string prodPath, std::string invPath, std::string prodInvPath,
		std::string _shopPath, std::string warehouses);
	// constructor for a warehouse
	FileLogger(std::string prodpath, std::string _warehousePath);
	void LoadShopProducts(CustomVector<ShopProduct*>& vec);
	void LoadWarehouseProducts(CustomVector<Product*>& vec);
	void LoadWarehouseList(CustomVector<std:: string>& warehouses);
	void SaveShopProducts(CustomVector<ShopProduct*>& prods, std::string ShopName);
	void SaveWarehouseProducts(CustomVector<Product*>& prods);
	void LoadInvoices(CustomVector<Invoice*>& vec);
	void SaveInvoices(CustomVector<Invoice*>& vec);
	static bool IsEmpty(std::string& path);
	void SaveShopName(Shop& s);
	static std::string& GetName();
};

#endif 
