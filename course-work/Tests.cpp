#include <exception>
#include <vector>

#include "Tests.h"
#include "FileLogger.h"
#include "Invoice.h"
#include "Shop.h"
#include "CustomVector.h"
#include "ConsoleUI.h"


void RunTests() {
	//TestProduct();
	//TestShopProduct();
	//TestPoly();
	//TestVector();
	//TestCustomVector();
	//TestFileLogger();
	//TestShop();

	std::cout << "\nEnd of tests\n";
}
void TestProduct() {

	// product constructors and date manipulation functions
	Product* p1 = new Product();
	std::cout << p1->GetDeliveryDate() << std::endl;
	p1->SetDateOfDeliveryCurrent();
	std::cout << p1->GetDeliveryDate() << std::endl;

	Product* prodConstr = new Product("p2", true, 100, "pc", 2, 5);
	std::cout << prodConstr->GetDeliveryDate() << std::endl;

	Product* prodToCpy = new Product(*(prodConstr));
	std::cout << prodToCpy->GetDeliveryDate();

	Product* prodDateManual = new Product("pman", "12.1.2022 12:24", true, 100, "pc", 10, 3);
	std::cout << prodDateManual->GetDeliveryDate() << std::endl;



	std::cout << "\nTestProduct completed successfully" << std::endl;
}
void TestShopProduct() {

	ShopProduct* p1 = new ShopProduct();
	ShopProduct* p2 = new ShopProduct("sp3", "01.01.2022 12:00", "10.01.2022 12:00",
		true, 100, "pc", 10, 10, 10);
	ShopProduct* p3 = new ShopProduct("pctor", true, 100, "pc", 10, 10);
	ShopProduct* p4 = new ShopProduct(*(p3));
	ShopProduct* pctor = new ShopProduct("pctor", true, 100, "pc", 10, 10);


	p4->ShopProduct::SetMarkup(10);

	std::cout << p4->ShopProduct::GetPrice() << "|" << p4->GetMarkup() << std::endl;

	std::cout << p1->GetExpiryDate() << std::endl;
	std::cout << p2->GetExpiryDate() << std::endl;
	std::cout << p3->GetExpiryDate() << std::endl;
	std::cout << p4->GetExpiryDate() << std::endl;

	Product* p = new Product();
	p->SetDateOfDeliveryCurrent();
	std::cout << "Del date: " << p->GetDeliveryDate();
	std::cout << "\n---------------\n";

	std::cout << " p3 Del date: " << p3->GetDeliveryDate() << std::endl;
	std::cout << " p3 exp date: " << p3->GetExpiryDate() << std::endl;
	std::cout << " p2 Del date: " << p2->GetDeliveryDate() << std::endl;
	std::cout << " p2 exp date: " << p2->GetExpiryDate() << std::endl;
	std::cout << " p3 exp date: " << p3->GetExpiryDate() << std::endl;
	std::cout << "\n---------------\n";

	std::cout << "pctor Del date: " << pctor->GetDeliveryDate() << std::endl;
	std::cout << "pctor Exp date: " << pctor->GetExpiryDate();


	std::cout << "\n-----copy test----------\n";
	//ShopProduct* pcopy = new ShopProduct(*pctor);
	ShopProduct* pcopy = new ShopProduct();
	pcopy = pctor;
	std::cout << "pcopy Del date: " << pcopy->GetDeliveryDate() << std::endl;
	std::cout << "pcopy Exp date: " << pcopy->GetExpiryDate();
	std::cout << "\nTestShopProduct completed successfully" << std::endl;
}
void TestPoly() {
	// test polymorphism and polymotphyc cluster

	
	std::cout << "\nTestPoly completed successfully" << std::endl;

}
void TestVector() {
	CustomVector<ShopProduct*> vec;

	vec.PushBack(new ShopProduct("p2", true, 100, "pc", 10, 10));
	vec.PushBack(new ShopProduct("p3", true, 100, "pc", 10, 10));
	vec.PushBack(new ShopProduct("p4", true, 100, "pc", 10, 10));
	vec.PushBack(new ShopProduct("p5", true, 100, "pc", 10, 10));
	vec.PushBack(new ShopProduct("p6", true, 100, "pc", 10, 10));



	//for (int i = 0; i < vec.Size(); i++) {
	//	vec.At(i)->SetDateOfDeliveryCurrent();
	//}
	//std::cout << vec.Size() << "|" << vec.Capacity() << std::endl;
	//for (int i = 0; i < vec.Size(); i++) {
	//	std::cout << vec.At(i)->GetDeliveryDate() << std::endl;
	//}
	//std::cout << "\n------------------\n";
	//
	//for (auto i : vec) {
	//	std::cout << i->GetDeliveryDate() << std::endl;
	//}
	//std::cout << "\n------------------\n";
	//vec.PushBack(new ShopProduct("p7", true, 100, "pc", 10, 10));
	//vec.PushBack(new ShopProduct("p8", true, 100, "pc", 10, 10));
	CustomVector<ShopProduct*> copyVec(vec);
	//copyVec.At(5);

	//ShopProduct* d = new ShopProduct();
	//Product* b = d;
	//delete b;

	//delete vec.At(0);
	//copyVec.Remove(0);
	//copyVec.Remove(0);
	//copyVec.Remove(0);
	//copyVec.Remove(0);
	// mem
	for (int i = 0; i < vec.GetSize(); i++) {
		std::cout << vec.At(i)->GetTitle() << "|" << vec.At(i) << std::endl;
	}
	std::cout << "\n------------------\n";
	for (int i = 0; i < copyVec.GetSize(); i++) {
		std::cout << copyVec.At(i)->GetTitle() << "|" << copyVec.At(i) << std::endl;
	}
	std::cout << "\n------------------\n";
	// mem
	//copyVec = vec;

	// mem
	for (int i = 0; i < vec.GetSize(); i++) {
		std::cout << vec.At(i)->GetTitle() << "|" << vec.At(i) << std::endl;
	}
	std::cout << "\n------------------\n";
	for (int i = 0; i < copyVec.GetSize(); i++) {
		std::cout << copyVec.At(i)->GetTitle() << "|" << copyVec.At(i) << std::endl;
	}
	std::cout << "\n------------------\n";
	// mem

	//vec.Clear();

	std::cout << "\n------------------\n";

	// mem
	for (int i = 0; i < vec.GetSize(); i++) {
		std::cout << vec.At(i)->GetTitle() << "|" << vec.At(i) << std::endl;
	}
	std::cout << "\n------------------\n";
	for (int i = 0; i < copyVec.GetSize(); i++) {
		std::cout << copyVec.At(i)->GetTitle() << "|" << copyVec.At(i) << std::endl;
	}
	std::cout << "\n------------------\n";
	// mem
	for (int i = 0; i < vec.GetSize(); i++) {
		std::cout << copyVec.At(i)->GetTitle() << "|" << copyVec.At(i)->GetDeliveryDate() << std::endl;
	}

	std::cout << vec.GetSize() << "|" << vec.GetCapacity() << std::endl;
	vec.Remove(0);

	for (int i = 0; i < vec.GetSize(); i++) {
		std::cout << vec.At(i)->GetTitle() << "|" << vec.At(i) << std::endl;
	}
	vec.Remove(1);
	vec.Remove(vec.GetSize() - 1);
	for (int i = 0; i < vec.GetSize(); i++) {
		std::cout << vec.At(i)->GetTitle() << "|" << vec.At(i)->GetDeliveryDate() << std::endl;
	}
	std::cout << vec.GetSize() << "|" << vec.GetCapacity() << std::endl;
	vec.PushBack(new ShopProduct("p11", true, 100, "pc", 10, 10));
	vec.PushBack(new ShopProduct("p12", true, 100, "pc", 10, 10));
	for (int i = 0; i < vec.GetSize(); i++) {
		std::cout << vec.At(i)->GetTitle() << "|" << vec.At(i)->GetDeliveryDate() << std::endl;
	}
	std::cout << vec.GetSize() << "|" << vec.GetCapacity() << std::endl;


	std::cout << "\n------------------\n";

	std::cout << "\nClearing...\n";
	std::cout << vec.GetSize() << std::endl;

	std::cout << vec.At(0)->GetTitle() << std::endl;
	vec.Remove(3);
	std::cout << vec.At(0)->GetTitle() << std::endl;
	vec.Clear();
	for (int i = 0; i < vec.GetSize(); i++) {
		std::cout << vec.At(i)->GetTitle() << "|" << vec.At(i)->GetDeliveryDate() << std::endl;
	}

	for (int i = 0; i < vec.GetSize(); i++) {
		std::cout << copyVec.At(i)->GetTitle() << "|" << copyVec.At(i)->GetDeliveryDate() << std::endl;
	}
	std::cout << "\n------------------\n";
	//for()
	copyVec.Clear();
	std::cout << copyVec.GetSize();

	for (int i = 0; i < copyVec.GetSize(); i++) {
		std::cout << copyVec.At(i)->GetTitle() << "|" << copyVec.At(i)->GetDeliveryDate() << std::endl;
	}
	std::cout << "\nTestVector completed successfully" << std::endl;
}
void TestCustomVector() {
	CustomVector<ShopProduct> vec1;
	CustomVector<Product> vec2;

	vec1.PushBack(ShopProduct("p4", true, 100, "pc", 10, 10));
	vec1.PushBack(ShopProduct("p5", true, 100, "pc", 10, 10));
	vec1.PushBack(ShopProduct("p6", true, 100, "pc", 10, 10));

	vec2.PushBack(Product("p2", true, 100, "pc", 10, 10));
	vec2.PushBack(Product("p3", true, 100, "pc", 10, 10));



	//vec1.PushBack(vec2.At(1));


	vec2.Remove(0);
	//std::cout << vec->At(2)->GetExpiryDate() << std::endl;

	//vec->Remove(0);
	//for (auto i : vec) {
	//	std::cout << i.GetExpiryDate() << std::endl;
	//}
	//vec->Clear();
	vec1.Clear();


}
void TestFileLogger() {

	FileLogger logger("products.txt", "invoices.txt", "invoice-products.txt",
		"course-work/Data/Shop/", "course-work/Data/Warehouse/warehouses.txt");
	CustomVector<ShopProduct*> collection;
	FileLogger wareLogger("Warehouse1-products.txt", "course-work/Data/Warehouse/");
	CustomVector<Product*> ware;
	//std::string path = "course-work/Data/Warehouse/Warehouse1-products.txt";
	//Warehouse ware("Warehouse1");
	logger.LoadShopProducts(collection);
	//logger.SaveShopProducts(&collection);
	wareLogger.LoadWarehouseProducts(ware);
	ware.Remove(0);
	//for (int i = 0; i < ware.GetSize(); i++) {
	//	std::cout << ware.At(i)->GetTitle() << "|" << ware.At(i)->GetDeliveryDate() << std::endl;
	//}
	//wareLogger.LoadWarehouseProducts(&wareCol);
	//wareLogger.SaveWarehouseProducts(ware);
	//for (auto i : collection) {
	//	std::cout << i->GetTitle() << "|" << i->GetDeliveryDate() << "|"
	//		<< static_cast<ShopProduct*>(i)->GetExpiryDate() << "|" << i->GetQuality() << "|" << i->GetPrice() << std::endl;
	//}
	//std::cout << "\n----------------------------------------------\n";
	//for (auto i : ware) {
	//	std::cout << i-:GetTitle() << "|" << i->GetDeliveryDate() 
	//		<< "|" << i->GetPrice() << std::endl;

	//std::cout << "\n----------------------------------------------\n";

	//// invoice test
	//
	//tm* date = new tm();
	//ProductVector vec;
	//vec.PushBack(new Product("p1", true, 100, "pc", 10, 10));
	//vec.PushBack(new Product("p2", true, 100, "pc", 10, 10));
	//vec.PushBack(new ShopProduct("p3", true, 100, "pc", 10, 10));
	//vec.PushBack(new ShopProduct("p4", true, 100, "pc", 10, 10));
	//vec.PushBack(new ShopProduct("p5", true, 100, "pc", 10, 10));
	//vec.PushBack(new ShopProduct("p6", true, 100, "pc", 10, 10));


	//Invoice* inv = new Invoice(new ProductVector(vec), "s1", "b1", date);



	CustomVector<Invoice*> invVec;
	CustomVector<std::string> wareVec;

	Shop s("MyShop");


	for (auto i : ware) {
		std::cout << i << std::endl;
	}
	std::cout << "\n----------------------------------------------\n";
	for (auto i : invVec) {
		std::cout << i->GetId() << "|" << i->GetSeller() << "|" << i->GetBuyer() << "|" << i->GetDate() << std::endl;
	}
	std::cout << "\n----------------------------------------------\n";

	logger.SaveInvoices(invVec);
	//invVec.clear();
	for (auto i : invVec) {
		std::cout << i->GetId() << "|" << i->GetSeller() << "|" << i->GetBuyer() << "|" << i->GetDate() << std::endl;
	}
	std::cout << "\n----------------------------------------------\n";
}
void TestShop() {
	Shop s("MyShop");
	s.InitialyzeWarehouse("Warehouse1");
	ConsoleUI ui(s);
	//ui.MainMenu();
	ui.DisplayShop();
	ui.DisplayWarehouse();
	//int input = 1;
	s.AddItemToOrder(0, 5);
	s.AddItemToOrder(2, 9);
	s.OrderFromWarehouse();
	s.AddItemToSellList(0, 5);
	s.Sell("Buyer1");
	ui.DisplayWarehouse();
	//s.CancelOrder();
	//input = 2;
	//s.AddItemToSellList(1, 10);
	ui.DisplayShop();
	//s.CancelSell();
	ui.DisplayShop();

}
 