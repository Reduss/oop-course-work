#include "ShopProductVector.h"

void ShopProductVector::allocate(size_t newCapacity) {
	ShopProduct* newData = new ShopProduct[newCapacity];

	if (newCapacity < this->size)
		this->size = newCapacity;

	for (int i = 0; i < this->size; i++) {
		newData[i] = std::move(this->data[i]);
	}
	delete[] data;

	this->data = newData;
	this->capacity = newCapacity;
}
void ShopProductVector::PopBack() {
	if (size > 0) {
		size--;
		data[size].~ShopProduct();
	}
}
ShopProductVector::ShopProductVector(){

}
ShopProductVector::ShopProductVector(ShopProductVector& vec){
	allocate(vec.size + 5);
	for (int i = 0; i < vec.size; ++i) {
		this->size++;
	}
}
void ShopProductVector::PushBack(ShopProduct* element) {

}
void ShopProductVector::Remove(int index) {

}
void ShopProductVector::Clear(){
	while (size != 0)
		PopBack();
	this->capacity = 0;
}
size_t ShopProductVector::Size() {
	return this->size;
}
size_t ShopProductVector::Capacity() {
	return this->capacity;
}
Product& ShopProductVector::At(int index) {
	return this->data[index];
}
ShopProductVector::~ShopProductVector() {
	Clear();
	delete data;
}
