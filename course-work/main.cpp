﻿#define _CRT_SECURE_NO_WARNINGS

#include <exception>

#include "Tests.h"
#include "ShopController.h"

int main()
{
    ShopController controller;

    try {
        controller.Start();
    }
    catch (const std::exception& ex) {
        std::cout << "\nError: ";
        std::cout << ex.what() << std::endl;
    }

}