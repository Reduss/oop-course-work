#define _CRT_SECURE_NO_WARNINGS
#include "utils.h"

#include <ctime>
#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>

void strTotm(std::string& str, tm* t){

	int dd, mm, yy, hh, min;

	std::istringstream ss(str);
	ss >> std::get_time(t, "%d.%m.%Y %H:%M");
}
std::string tmTostr(tm* time) {
	if (time == nullptr)
		return "";
	char str[100];
	strftime(str, 100, "%d.%m.%Y %H:%M", time);
	std::string toReturn(str);
	return toReturn;
}
void setTimeCur(tm& time) {
	time_t t = std::time(nullptr);
	time = *localtime(&t);
}
bool IsNum(std::string str) {
	for (int i = 0; i < str.length(); i++) {
		if (std::isdigit(str[i]) == false) {
			return false;
		}
	}
	return true;
}
int getInt() {
    while (1) {
        int x;  
        std::cin >> x;

        if (std::cin.fail() || (x > std::numeric_limits<int>::max()) ||
			(x < std::numeric_limits<int>::min())) {
            std::cout << "Invalid input.\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else
            return x;
    }
}