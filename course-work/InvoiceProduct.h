#ifndef INVOICE_PRODUCT
#define INVOICE_PRODUCT

#include "Product.h"

// describes a product in an invoice
class InvoiceProduct : private Product {
public:
	Product::SetTitle;
	Product::SetAmount;
	Product::SetPrice;


	Product::GetTitle;
	Product::GetAmount;
	Product::GetPrice;

	InvoiceProduct(std::string title, int amount, double price);
	explicit InvoiceProduct(Product* prod);
};
#endif 
