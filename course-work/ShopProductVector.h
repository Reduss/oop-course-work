#ifndef SHOP_PROD_VEC
#define SHOP_PROD_VEC

#include "ShopProduct.h"

class ShopProductVector {
private:
	ShopProduct* data = nullptr;
	size_t size = 0;
	size_t capacity = 0;
	void allocate(size_t newCapacity);
	void PopBack();
public:
	// default constructor
	ShopProductVector();
	// copy constructor
	ShopProductVector(ShopProductVector& vec);
	// adds an element to the back of the vector
	void PushBack(ShopProduct* element);
	// removes the element at a specified index
	void Remove(int index);
	// clears the container
	void Clear();
	size_t Size();
	size_t Capacity();
	// returns the elements at a specified index
	Product& At(int index);

	~ShopProductVector();
};


#endif