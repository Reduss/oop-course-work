#ifndef PRODUCT
#define PRODUCT

#include <iostream>
#include <string>
#include <ctime>


// describes a product in a warehouse
class Product {
protected:
	std::string title;
	tm* dateOfDelivery;
	bool qualityCertificate;
	double price;
	std::string unit; 
	int amount;
	int expiry; // how many days the product can be stored in the shop
public:
	//default constuctor
	Product();
	// parameterized constructor, 
	// the delivery date is set automatically ot the current date
	Product(std::string tit, 
		bool quality, double price, std::string unit, int amount, int expiry);
	// parameterized constructor, the delivery date is set manualy
	// date format: dd.mm.yyyy hh:mm
	Product(std::string tit, std::string deliveryDate,
		bool quality, double price, std::string unit, int amount, int expiry);
	// Copy constructor
	Product(const Product& prodToCopy);
	// Destructor
	virtual ~Product();
	

	void SetTitle(std::string newTitle);
	void SetPrice(double newPrice);
	void SetAmount(int newAmount);
	// if the certificate is false, sets it to true
	void SetQualityCertificate();
	// sets dateOfDelivery to the current time and date
	void SetDateOfDeliveryCurrent();
	

	std::string GetTitle();
	// returns a string representation of dateOfDelivery, 
	// if dateOfDelivery is nullptr, returns empty string
	std::string GetDeliveryDate();
	//returns true if a product has a quality certificate
	bool CheckQuality();
	// returns a string representations of QualitySertificate
	std::string GetQualityStr();
	bool GetQuality();
	virtual double GetPrice();
	// returns the measuring units of a product
	std::string GetUnits();
	int GetAmount();
	int GetExpiry();


	// copy assignment operator
	Product& operator=(const Product& prod);
};
#endif 
