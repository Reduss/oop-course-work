#define _CRT_SECURE_NO_WARNINGS

#include "ShopProduct.h"
#include "utils.h"
#include <iomanip>

void ShopProduct::setExpiryDate() {

	this->expiryDate = new tm;
	
	time_t raw;
	std::time(&raw);
	*(this->expiryDate) = *std::localtime(&raw);

	int toAdd = this->expiry;
	time_t interval = mktime(expiryDate);
	interval += 60 * 60 * 24 * toAdd;
	*(expiryDate) = *std::localtime(&interval); 
}
ShopProduct::ShopProduct() : Product(), markupPercentage(0), expiryDate(nullptr) {}
ShopProduct::ShopProduct(std::string tit, std::string delivDate, std::string expDate, 
	bool quality, double price, std::string unit, int amount, int exp, int mark):
		Product(tit, delivDate, quality, price, unit,amount, exp), markupPercentage(mark) {

	this->expiryDate = new tm;
	this->dateOfDelivery = new tm;
	strTotm(delivDate, this->dateOfDelivery);
	strTotm(expDate, (this->expiryDate));
}
ShopProduct::ShopProduct(std::string tit, 
	bool quality, double price, std::string unit, int amount, int expire) : Product(tit, quality,
		price, unit, amount, expire), markupPercentage(0), expiryDate(nullptr) {

	this->setExpiryDate();
}
ShopProduct::ShopProduct(const ShopProduct& prod) : Product(prod),
				 markupPercentage(0), expiryDate(nullptr) {
	this->expiryDate = new tm();
	*(this->expiryDate) = *(prod.expiryDate);
	this->markupPercentage = prod.markupPercentage;
}
ShopProduct& ShopProduct::operator=(const ShopProduct& prod) {
	if (this != &prod) {
		Product::operator=(prod);
		this->markupPercentage = prod.markupPercentage;
		delete this->expiryDate;
		if (prod.expiryDate != nullptr) {
			this->expiryDate = new tm();
			*(this->expiryDate) = *(prod.expiryDate);
		}
	}
	return *this;
}
ShopProduct::~ShopProduct() {
	if (this->expiryDate != nullptr) {
		delete this->expiryDate;
		this->expiryDate = nullptr;
	}
}

int ShopProduct::GetMarkup() {
	return this->markupPercentage;
}

double ShopProduct::GetPrice() {
	return this->price + ((this->price * this->markupPercentage) / 100);
}
double ShopProduct::GetOriginalPrice() {
	return this->price;
}
std::string ShopProduct::GetExpiryDate() {
	return tmTostr((this->expiryDate));
}
void ShopProduct::SetMarkup(int val) {
	this->markupPercentage = val;
}