#define _CRT_SECURE_NO_WARNINGS

#include "Product.h"
#include <iomanip>
#include "utils.h"

Product::Product() : title(""), dateOfDelivery(nullptr),
	qualityCertificate(false), price(0), unit(""), amount(0), expiry(0) { }
Product::Product(std::string tit, bool quality, double price_, std::string unit_,
	int amount_, int expiry_): title(tit), dateOfDelivery(nullptr), qualityCertificate(quality), 
	price(price_), unit(unit_), amount(amount_), expiry(expiry_) {

	SetDateOfDeliveryCurrent();
}
Product::Product(std::string tit, std::string date, 
	bool quality, double price_, std::string unit_, int amount_, int expiry_) : title(tit), dateOfDelivery(nullptr), qualityCertificate(quality),
	price(price_), unit(unit_), amount(amount_), expiry(expiry_) {

	this->dateOfDelivery = new tm;
	strTotm(date, dateOfDelivery);
}
Product::Product(const Product& prodToCopy) {
	this->title = prodToCopy.title;
	this->dateOfDelivery = new tm();
	*(this->dateOfDelivery) = *(prodToCopy.dateOfDelivery);
	this->price = prodToCopy.price;
	this->unit = prodToCopy.unit;
	this->amount = prodToCopy.amount;
}
Product::~Product() {
	if (this->dateOfDelivery != nullptr) {
		delete this->dateOfDelivery;
		dateOfDelivery = nullptr;
	}
}
void Product::SetTitle(std::string newTitle) {
	this->title = move(newTitle);
}
void Product::SetPrice(double newPrice) {
	this->price = newPrice;
}
void Product::SetAmount(int newAmount) {
	this->amount = newAmount;
}
void Product::SetQualityCertificate() {
	if (this->qualityCertificate == false)
		this->qualityCertificate = true;
}
void Product::SetDateOfDeliveryCurrent() {
	this->dateOfDelivery = new tm();
	setTimeCur(*dateOfDelivery);
}
std::string Product::GetTitle() {
	return this->title;
}
std::string Product::GetDeliveryDate() {
	return tmTostr(this->dateOfDelivery);
}
bool Product::CheckQuality() {
	return this->qualityCertificate;
}
std::string Product::GetQualityStr(){
	if (qualityCertificate)
		return "yes";
	else
		return "no";
}
bool Product::GetQuality() {
	return this->qualityCertificate;
}
double Product::GetPrice() {
	return this->price;
}
std::string Product::GetUnits() {
	return this->unit;
}
int Product::GetAmount() {
	return this->amount;
}
int Product::GetExpiry() {
	return this->expiry;
}
Product& Product::operator=(const Product& prod) {
	if (this != &prod) {
		this->title = prod.title;
		delete this->dateOfDelivery;
		if (prod.dateOfDelivery != nullptr) {
			this->dateOfDelivery = new tm();
			*(this->dateOfDelivery) = *(prod.dateOfDelivery);
		}
		this->qualityCertificate = prod.qualityCertificate;
		this->price = prod.price;
		this->unit = prod.unit;
		this->price = prod.price;
		this->amount = prod.amount;
		this->expiry = prod.expiry;
	}
	return *this;
}
