#include "InvoiceProduct.h"

InvoiceProduct::InvoiceProduct(std::string title, int amount, double price) {
	this->SetTitle(title);
	this->SetAmount(amount);
	this->SetPrice(price);
}
InvoiceProduct::InvoiceProduct(Product* prod) {
	this->SetTitle(prod->GetTitle());
	this->SetAmount(prod->GetAmount());
	this->SetPrice(prod->GetPrice());
}
