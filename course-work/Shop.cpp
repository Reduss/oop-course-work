#include "Shop.h"
#include "FileLogger.h"
#include "utils.h"

// TOOD: proift and other money calculations


void Shop::LoadFromFile() {
	
	this->logger->LoadShopProducts(this->collection);
	this->logger->LoadInvoices(this->invoices);
	this->logger->LoadWarehouseList(this->warehouses);
}
void Shop::SaveToFile() {
	this->logger->SaveShopName(*this);
	this->logger->SaveShopProducts(this->collection, this->name);
	this->logger->SaveInvoices(this->invoices);
	if(this->warehouse != nullptr)
		this->warehouse->Save();
}
CustomVector<Invoice*>& Shop::GetCurentMonthInvoices()
{
	tm* curTime = new tm;
	setTimeCur(*curTime);
	CustomVector<Invoice*> *vec = new CustomVector<Invoice*>();


	for (auto i : this->invoices) {
		if (i->GetTmDate().tm_year == curTime->tm_year && i->GetTmDate().tm_mon == curTime->tm_mon)
			vec->PushBack(new Invoice(*i));
	}
	return *vec;
}
Shop::Shop(std::string name) : name(name), warehouse(nullptr), logger(nullptr),
		moneyEarned(0), moneySpent(0), profit(0) {

	this->logger = new FileLogger("products.txt", "invoices.txt", "invoice-products.txt",
		"course-work/Data/Shop/", "course-work/Data/Warehouse/warehouses.txt");
	if (FileLogger::IsEmpty(std::string("course-work/Data/Shop/products.txt")))
		logger->SaveShopName(*this);
	this->LoadFromFile();
}
ShopProduct& Shop::GetElement(int index) {
	return *this->collection.At(index);
}
CustomVector<ShopProduct*>& Shop::GetProducts() {
	return this->collection;
}
CustomVector<ShopProduct*>& Shop::GetToSellList() {
	return this->currentToSellList;
}
CustomVector<Product*>& Shop::GetOrderList() {
	return this->currentOrder;
}
Warehouse& Shop::GetWarehouse()
{
	return *this->warehouse;
}
CustomVector<Invoice*>& Shop::GetInvoices() {
	return this->invoices;
}
CustomVector<std::string>& Shop::GetWarehouses()
{
	return this->warehouses;
}
void Shop::InitialyzeWarehouse(std::string name) {
	if (warehouse != nullptr)
		delete warehouse;
	this->warehouse = new Warehouse(name);
}
int Shop::AddItemToOrder(int index, int amount) {
	if (index < 0 || index >= warehouse->GetSize())
		return 0;
	if (amount > this->warehouse->GetElement(index)->GetAmount())
		return 0;
	if (amount == this->warehouse->GetElement(index)->GetAmount()) {
		// add a new ShopProduct based on Product at index from the warehouse
		this->currentOrder.PushBack(new ShopProduct(
			this->warehouse->GetElement(index)->GetTitle(), 
			this->warehouse->GetElement(index)->GetQuality(),
			this->warehouse->GetElement(index)->GetPrice(),
			this->warehouse->GetElement(index)->GetUnits(),
			this->warehouse->GetElement(index)->GetAmount(),
			this->warehouse->GetElement(index)->GetExpiry()));
		// remove the product from the warehouse
		this->warehouse->GetCollection().Remove(index);
		return 1;
	}
	if (amount < this->warehouse->GetElement(index)->GetAmount()) {
		// add a new ShopProduct based on Product at index from the warehouse
		this->currentOrder.PushBack(new ShopProduct(
			this->warehouse->GetElement(index)->GetTitle(),
			this->warehouse->GetElement(index)->GetQuality(),
			this->warehouse->GetElement(index)->GetPrice(),
			this->warehouse->GetElement(index)->GetUnits(),
			amount,						// set the amount of ShopProduct to the one cpecified as an argument
			this->warehouse->GetElement(index)->GetExpiry()));

		//this->currentOrder.At(this->currentOrder.GetSize() - 1)->SetAmount(amount);

		int newAmount = this->warehouse->GetElement(index)->GetAmount() - amount;
		this->warehouse->GetElement(index)->SetAmount(newAmount);
	}
	return 1;
}
int Shop::AddItemToSellList(int index, int amount) {
	if (index < 0 || index >= collection.GetSize())
		return 0;
	if (amount > this->collection.At(index)->GetAmount())
		return 0;
	if (amount == this->collection.At(index)->GetAmount()) {
		this->currentToSellList.PushBack(new ShopProduct(*this->collection.At(index)));
		this->collection.Remove(index);
		return 1;
	}
	if (amount < this->collection.At(index)->GetAmount()) {
		// add to ToSell list
		this->currentToSellList.PushBack(new ShopProduct(*this->collection.At(index)));
		// set the amount of added product to "amount"
		this->currentToSellList.At(this->GetToSellList().GetSize() - 1)->SetAmount(amount);
		// calculate the new amount of the product in the shop
		int newAmount = this->collection.At(index)->GetAmount() - amount;
		// set the new amount for the shop product		
		this->collection.At(index)->SetAmount(newAmount);
	}
	return 1;
}
int Shop::OrderFromWarehouse() {
	this->invoices.PushBack(new Invoice(this->warehouse->GetName(), this->GetName()));
	if (this->currentOrder.GetSize() == 0)
		return 0;
	while (this->currentOrder.GetSize() != 0) {
		// add the product to the invoice
		this->invoices.At(this->invoices.GetSize() - 1)->AddProduct(new InvoiceProduct(currentOrder.At(0)->GetTitle(),
			currentOrder.At(0)->GetAmount(),
			currentOrder.At(0)->GetPrice()));
		this->collection.PushBack(static_cast<ShopProduct*>(currentOrder.At(0)));
		this->currentOrder.Remove(0);
	}
	this->SaveToFile();
	return 1;
}
void Shop::CancelOrder() {
	if (this->currentOrder.GetSize() == 0)
		return;
	bool flag;
	while(this->currentOrder.GetSize() != 0) {
		flag = false;
		for (int j = 0; j < this->warehouse->GetSize(); j++) {
			// if there is already such a product in the warehouse, summ their amounts
			if (this->currentOrder.At(0)->GetTitle() == this->warehouse->GetElement(j)->GetTitle()) {
				flag = true;
				int newAmount = this->currentOrder.At(0)->GetAmount() + this->warehouse->GetElement(j)->GetAmount();
				this->warehouse->GetElement(j)->SetAmount(newAmount);
				this->currentOrder.Remove(0);
				break;
			}
		}
		// if no match is found, add new product and delete from order
		if (!flag) {
			this->warehouse->AddElement(currentOrder.At(0));
			this->currentOrder.Remove(0);
		}
	}
}
void Shop::Sell(std::string buyerName) {
	if (this->currentToSellList.GetSize() == 0)
		return;
	this->invoices.PushBack(new Invoice(this->GetName(), buyerName));
	for (auto i : currentToSellList) {
		this->invoices.At(this->invoices.GetSize() - 1)->AddProduct(new InvoiceProduct(i->GetTitle(),
			i->GetAmount(),
			i->GetPrice()));
	}
	this->currentToSellList.Clear();
	this->SaveToFile();
}
void Shop::CancelSell() {	
	if (this->currentToSellList.GetSize() == 0)
		return;
	bool flag;
	while(this->currentToSellList.GetSize() != 0) {
		flag = false;
		for (int j = 0; j < this->collection.GetSize(); j++) {
			// if there is already such a product in the shop, summ their amounts
			if (this->currentToSellList.At(0)->GetTitle() == this->collection.At(j)->GetTitle()) {
				flag = true;
				int newAmount = this->currentToSellList.At(0)->GetAmount() + this->collection.At(j)->GetAmount();
				this->collection.At(j)->SetAmount(newAmount);
				this->currentToSellList.Remove(0);
				break;
			}
		}
		if (!flag) {

			// if no match is found, add new product and delete from sellList
			this->collection.PushBack(new ShopProduct(*currentToSellList.At(0)));
			this->currentToSellList.Remove(0);
		}
	}
}
std::string& Shop::GetName() {
	return this->name;
}
double Shop::GetExpences()
{
	return this->moneySpent;
}
double Shop::GetEarnings()
{
	return this->moneyEarned;
}
double Shop::GetProfit()
{
	return this->profit;
}
void Shop::CalculateBudget(CustomVector<Invoice*>& invs) {
	for (auto i : invs) {
		if (i->GetSeller() == this->name)
			moneyEarned += i->GetTotalPrice();
		if (i->GetBuyer() == this->name)
			moneySpent += i->GetTotalPrice();
	}
	this->profit = moneyEarned - moneySpent;
}
void Shop::SetName(std::string name) {
	this->name = name;
}
Shop::~Shop() {
	this->SaveToFile();
}

